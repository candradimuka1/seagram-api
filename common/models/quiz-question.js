'use strict';

const app = require('../../server/server');

module.exports = function(Quizquestion) {
  Quizquestion.getQuestion = function(id, cb) {
    const Question = app.models.Question;
    Quizquestion.findOne({
      where: {id: id},
    }, (err, quizQuestion) => {
      if (err) return cb(err);
      if (!quizQuestion) {
        return cb('quiz-question not found');
      }
      Question.findOne({
        where: {id: quizQuestion.questionId},
      }, (err, question) => {
        if (err) return cb(err);
        if (!question) {
          return cb('question not found');
        }
        cb(null, question);
      });
    });
  };
};
