'use strict';

const app = require('../../server/server');

module.exports = function(Lecturedetails) {
  Lecturedetails.getLectureDetailsAndUrl = function(id, cb) {
    const Material = app.models.Material;
    Lecturedetails.findOne({
      where: {id: id},
    }, (err, lectureDetails) => {
      if (err) return cb(err);
      if (!lectureDetails) {
        return cb('lecture details not found');
      }
      Material.findOne({
        where: {id: lectureDetails.materialId},
      }, (err, material) => {
        if (err) return cb(err);
        if (!material) {
          return cb('material not found');
        }
        const ret = {
          id: lectureDetails.id,
          lectureId: lectureDetails.lectureId,
          description: lectureDetails.description,
          url: material.url,
        };
        cb(null, ret);
      });
    });
  };
  Lecturedetails.postLectureDetails = function(lectureId, data, cb) {
    const Material = app.models.Material;
    Material.create({
      url: data.url,
    }, (err, material) => {
      if (err) return cb(err);
      Lecturedetails.create({
        lectureId: lectureId,
        materialId: material.id,
        description: data.description,
      }, (err, lectureDetail) => {
        if (err) return cb(err);
        cb(null, lectureDetail);
      });
    });
  };
  Lecturedetails.deleteLectureDetails = function(id, cb) {
    const Material = app.models.Material;
    Lecturedetails.findOne({
      where: {id: id},
    }, (err, lectureDetail) => {
      if (err) return cb(err);
      if (!lectureDetail) return cb(null);
      Material.findOne({
        where: {id: lectureDetail.materialId},
      }, (err, material) => {
        if (err) return cb(err);
        if (!material) return cb(null);
        Material.destroyById(material.id, (err) => {
          if (err) return cb(err);
          Lecturedetails.destroyById(id, (err) => {
            if (err) return cb(err);
            cb(null);
          });
        });
      });
    });
  };
  Lecturedetails.patchLectureDetails = function(lectureDetails, data, cb) {
    const Material = app.models.Material;
    const lectureDetailsData = {
      description: data.description || lectureDetails.description,
    };
    lectureDetails.patchAttributes(
      lectureDetailsData,
      (err, lectureDetails) => {
        if (err) return cb(err);
        lectureDetails.__get__material((err, material) => {
          if (err) return cb(err);
          if (!material) return cb('material not found');
          Material.patchMaterial(material, data, (err) => {
            if (err) return cb(err);
            cb(null);
          });
        });
      }
    );
  };
};
