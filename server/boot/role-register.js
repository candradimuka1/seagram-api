'use strict';

module.exports = function(app) {
    function findOrCreateRole(name){
        var Role = app.models.Role;
        Role.findOne({"where": {"name": name}},function(err,results){
            if (err) {console.log(err);}
            if (!results){
                Role.create({"name":name})
            }
        });
    }
    findOrCreateRole("admin");
    findOrCreateRole("student");
    findOrCreateRole("instructor");
};