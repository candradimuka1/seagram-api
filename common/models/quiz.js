'use strict';

const app = require('../../server/server');
const async = require('async');

module.exports = function(Quiz) {
  Quiz.getQuestions = function(id, cb) {
    const QuizQuestion = app.models.QuizQuestion;
    Quiz.findOne({
      where: {id: id},
    }, (err, quiz) => {
      if (err) return cb(err);
      if (!quiz) {
        return cb('quiz not found');
      }
      QuizQuestion.find({
        where: {quizId: quiz.id},
      }, (err, quizQuestions) => {
        if (err) return cb(err);
        async.mapSeries(quizQuestions, (quizQuestion, cbAsync) => {
          QuizQuestion.getQuestion(quizQuestion.id, (err, question) => {
            if (err) return cbAsync(err);
            cbAsync(null, question);
          });
        }, (err, questions) => {
          if (err) return cb(err);
          cb(null, questions);
        });
      });
    });
  };
  Quiz.remoteMethod('getQuestions', { // TODO: remove this
    http: {path: '/:id/questions', verb: 'get'},
    accepts: {arg: 'id', type: 'string', required: true},
    returns: {arg: 'questions', type: ['object']},
  });
};
