'use strict';

const app = require('../../server/server');
const async = require('async');

module.exports = function(Baseuser) {
  Baseuser.disableRemoteMethodByName('upsert');
  Baseuser.disableRemoteMethodByName('replaceById');

  // var app;
  var path = require('path');
  app.on('started', function() {
    // app = a;
    var Role = app.models.Role;
    var RoleMapping = app.models.RoleMapping;
    var Notification = app.models.Notification;
    // perform any setup that requires the app object
    Baseuser.observe('after save', function(ctx, next) {
      if (ctx.instance) {
        if (ctx.isNewInstance) {
          var newUserRole = ctx.instance.role;
          Role.findOne({where: {name: newUserRole}}, function(err, role) {
            if (err) { return next(err); }
            RoleMapping.create({
              principalType: 'BaseUser',
              principalId: ctx.instance.id,
              roleId: role.id,
            }, function(err, roleMapping) {
              if (err) { return next(err); }
              console.log(`User assigned RoleID ${role.id} (${role.name})`);
            });
          });
          if (newUserRole == "instructor"){
            Notification.create(
              {
                "title" : "A new Instructor is waiting to be verified",
                "description": "A new user with username " + ctx.instance.name + " is waiting to be verified",
                "notificationType": "instructor",
                "instructorId": ctx.instance.id,
                "courseId": "-"
              }
            )
          }
        }
      }
      next();
    });
  });
  var senderAddress = 'seagram.sentosa@gmail.com';
  // send verification email after registration
  Baseuser.afterRemote('create', function(context, user, next) {
    var options = {
      type: 'email',
      to: user.email,
      from: senderAddress,
      protocol: 'https',
      port: '443',
      host: 'seagram-api.herokuapp.com',
      subject: 'Thanks for registering.',
      template: path.resolve(__dirname, '../../server/views/verify.ejs'),
      redirect: 'https://seagram-app.herokuapp.com/verified',
      user: user,
    };
    user.verify(options, function(err, response) {
      if (err) {
        Baseuser.deleteById(user.id);
        return next(err);
      }
      next(null, response);
    });
  });

  Baseuser.beforeRemote('login', function(ctx,modelInstance,next) {
    var email = ctx.req.body.email;
    Baseuser.findOne({"where": {"email": email}},function(err,user){
      if (err) {next(err);}
      else if(user.instructorIsVerified == "pending" || user.instructorIsVerified == "unverified"){
        console.log("Instructor account not verified");
        next(new Error("Instructor account is " + user.instructorIsVerified));
      } else{
        next();
      }
    })

  });

  // Get Enrolled Courses
  Baseuser.getEnrolledCourses = function(req, cb) {
    const EnrolledCourse = app.models.EnrolledCourse;
    const userId = req.accessToken.userId;
    EnrolledCourse.find({
      where: {baseUserId: userId},
    }, (err, enrolledCourses) => {
      if (err) return cb(err);
      cb(null, enrolledCourses);
    });
  };
  Baseuser.remoteMethod('getEnrolledCourses', {
    http: {path: '/enrolledCourses', verb: 'get'},
    accepts: {arg: 'req', type: 'object', http: {source: 'req'}},
    returns: {arg: 'enrolledCourses', type: ['object'], http: {source: 'res'}},
  });
  // Post Enrolled Courses
  Baseuser.postEnrolledCourse = function(req, courseId, cb) {
    const Course = app.models.Course;
    const EnrolledCourse = app.models.EnrolledCourse;
    const userId = req.accessToken.userId;
    Course.findOne({
      where: {id: courseId},
    }, (err, course) => {
      if (err) return cb(err);
      if (!course || !(course.isVerified === 'verified')) {
        return cb('course not found');
      }
      EnrolledCourse.findOne({
        where: {
          baseUserId: userId,
          courseId: courseId,
        },
      }, (err, sameEnrolledCourse) => {
        if (err) return cb(err);
        if (sameEnrolledCourse != null) {
          return cb('user already enrolled in the course');
        }
        EnrolledCourse.create({
          baseUserId: userId,
          courseId: courseId,
          status: 'student',
        }, (err, enrolledCourse) => {
          if (err) return cb(err);
          cb(null, enrolledCourse);
        });
      });
    });
  };
  Baseuser.remoteMethod('postEnrolledCourse', {
    http: {path: '/enrolledCourses', verb: 'post'},
    accepts: [
      {arg: 'req', type: 'object', http: {source: 'req'}},
      {arg: 'courseId', type: 'string', required: true},
    ],
  });
  Baseuser.createInstructedCourse = function(req, body, cb) {
    const Course = app.models.Course;
    const Notification = app.models.Notification;
    const userId = req.accessToken.userId;
    if (!req.body.title || !req.body.description) {
      return cb('request is not valid');
    }
    Course.create({
      title: body.title,
      description: body.description,
      isVerified: 'pending',
      instructorId: userId,
    }, (err, course) => {
      if (err) return cb(err);
      cb(null, course);
      Notification.create({
        title: `New course created: ${body.title}`,
        description: body.description,
        isRead: false,
        notificationType: 'course',
        instructorId: '-',
        courseId: course.id,
      });
    });
  };
  Baseuser.remoteMethod('createInstructedCourse', {
    http: {path: '/instructedCourses', verb: 'post'},
    accepts: [
      {arg: 'req', type: 'object', http: {source: 'req'}},
      {arg: 'course', type: 'object', required: true, http: {source: 'body'}},
    ],
  });
  Baseuser.getInstructedCourse = function(req, cb) {
    const Course = app.models.Course;
    const userId = req.accessToken.userId;
    Course.find({
      where: {instructorId: userId},
    }, (err, courses) => {
      if (err) return cb(err);
      async.mapSeries(courses, (course, cbAsync) => {
        Course.countStudents(course.id, (err, count) => {
          if (err) return cbAsync(err);
          const courseDetails = {
            title: course.title,
            description: course.description,
            isVerified: course.isVerified,
            studentCount: count,
            id: course.id,
            instructorId: course.instructorId,
          };
          cbAsync(null, courseDetails);
        });
      }, (err, coursesDetails) => {
        if (err) return cb(err);
        cb(null, coursesDetails);
      });
    });
  };
  Baseuser.remoteMethod('getInstructedCourse', {
    http: {path: '/instructedCourses', verb: 'get'},
    accepts: {arg: 'req', type: 'object', http: {source: 'req'}},
    returns: {arg: 'courses', type: ['object']},
  });
  Baseuser.postLectureOnInstructedCourse = function(req, courseId, body, cb) {
    const Course = app.models.Course;
    const userId = req.accessToken.userId;
    if (!body.title || !body.estimatedTime || !body.description || !body.url) {
      return cb('request is not valid');
    }
    Course.findOne({
      where: {
        id: courseId,
        instructorId: userId,
      },
    }, (err, course) => {
      if (err) return cb(err);
      if (!course) return cb('course not found');
      Course.postLecture(course.id, body, (err, lecture) => {
        if (err) return cb(err);
        cb(null, lecture);
      });
    });
  };
  Baseuser.remoteMethod('postLectureOnInstructedCourse', {
    http: {path: '/instructedCourses/:courseId/lectures', verb: 'post'},
    accepts: [
      {arg: 'req', type: 'object', http: {source: 'req'}},
      {arg: 'courseId', type: 'string', required: true, http: {source: 'path'}},
      {arg: 'body', type: 'object', required: true, http: {source: 'body'}},
    ],
    returns: {arg: 'lecture', type: 'object'},
  });
  Baseuser.deleteLectureOnInstructedCourse = function(req, id, lectureId, cb) {
    const Course = app.models.Course;
    const userId = req.accessToken.userId;
    Course.findOne({
      where: {
        id: id,
        instructorId: userId,
      },
    }, (err, course) => {
      if (err) return cb(err);
      if (!course) return cb('course not found');
      Course.deleteLecture(course.id, lectureId, (err) => {
        if (err) return cb(err);
        cb(null);
      });
    });
  };
  Baseuser.remoteMethod('deleteLectureOnInstructedCourse', {
    http: {
      path: '/instructedCourses/:courseId/lectures/:lectureId',
      verb: 'delete',
    },
    accepts: [
      {arg: 'req', type: 'object', http: {source: 'req'}},
      {arg: 'courseId', type: 'string', required: true, http: {source: 'path'}},
      {
        arg: 'lectureId',
        type: 'string',
        required: true,
        http: {source: 'path'},
      },
    ],
  });
  Baseuser.getInstructedCourseDetails = function(req, id, cb) {
    const Course = app.models.Course;
    const userId = req.accessToken.userId;
    Course.findOne({
      where: {
        id: id,
        instructorId: userId,
      },
    }, (err, course) => {
      if (err) return cb(err);
      if (!course) return cb('course not found');
      cb(null, course);
    });
  };
  Baseuser.remoteMethod('getInstructedCourseDetails', {
    http: {
      path: '/instructedCourses/:courseId/',
      verb: 'get',
    },
    accepts: [
      {arg: 'req', type: 'object', http: {source: 'req'}},
      {arg: 'courseId', type: 'string', required: true, http: {source: 'path'}},
    ],
    returns: {arg: 'course', type: 'object'},
  });
  Baseuser.getInsCourseLectures = function(req, id, cb) {
    const Course = app.models.Course;
    const userId = req.accessToken.userId;
    Course.findOne({
      where: {
        id: id,
        instructorId: userId,
      },
    }, (err, course) => {
      if (err) return cb(err);
      if (!course) return cb('course not found');
      course.__get__lectures((err, lectures) => {
        if (err) return cb(err);
        cb(null, lectures);
      });
    });
  };
  Baseuser.remoteMethod('getInsCourseLectures', {
    http: {
      path: '/instructedCourses/:courseId/lectures',
      verb: 'get',
    },
    accepts: [
      {arg: 'req', type: 'object', http: {source: 'req'}},
      {arg: 'courseId', type: 'string', required: true, http: {source: 'path'}},
    ],
    returns: {arg: 'lectures', type: ['object']},
  });
  Baseuser.patchLectureOnInsCourse = function(req, id, lectureId, data, cb) {
    const Course = app.models.Course;
    const userId = req.accessToken.userId;
    Course.findOne({
      where: {
        id: id,
        instructorId: userId,
      },
    }, (err, course) => {
      if (err) return cb(err);
      if (!course) return cb('course not found');
      Course.patchLecture(course, lectureId, data, (err) => {
        if (err) return cb(err);
        cb(null);
      });
    });
  };
  Baseuser.remoteMethod('patchLectureOnInsCourse', {
    http: {
      path: '/instructedCourses/:courseId/lectures/:lectureId/',
      verb: 'patch',
    },
    accepts: [
      {arg: 'req', type: 'object', http: {source: 'req'}},
      {arg: 'courseId', type: 'string', required: true, http: {source: 'path'}},
      {
        arg: 'lectureId', type: 'string', required: true,
        http: {source: 'path'},
      },
      {arg: 'lecture', type: 'object', required: true, http: {source: 'body'}},
    ],
  });
  Baseuser.getLectureOnInsCourse = function(req, id, lectureId, cb) {
    const Course = app.models.Course;
    const userId = req.accessToken.userId;
    Course.findOne({
      where: {
        id: id,
        instructorId: userId,
      },
    }, (err, course) => {
      if (err) return cb(err);
      if (!course) return cb('course not found');
      Course.getLecture(course, lectureId, (err, lecture) => {
        if (err) return cb(err);
        if (!lecture) return cb('lecture not found');
        cb(null, lecture);
      });
    });
  };
  Baseuser.remoteMethod('getLectureOnInsCourse', {
    http: {
      path: '/instructedCourses/:courseId/lectures/:lectureId/',
      verb: 'get',
    },
    accepts: [
      {arg: 'req', type: 'object', http: {source: 'req'}},
      {arg: 'courseId', type: 'string', required: true, http: {source: 'path'}},
      {
        arg: 'lectureId', type: 'string', required: true,
        http: {source: 'path'},
      },
    ],
    returns: {arg: 'lecture', type: 'object'},
  });
  Baseuser.getLectureProgresses = function(req, cb) {
    const LectureProgress = app.models.LectureProgress;
    const userId = req.accessToken.userId;
    LectureProgress.find({
      where: {
        baseUserId: userId,
      },
    }, (err, lectureProgresses) => {
      if (err) return cb(err);
      cb(null, lectureProgresses);
    });
  };
  Baseuser.remoteMethod('getLectureProgresses', {
    http: {
      path: '/lectureProgresses/',
      verb: 'get',
    },
    accepts: {arg: 'req', type: 'object', http: {source: 'req'}},
    returns: {arg: 'lectureProgresses', type: ['object']},
  });
  Baseuser.postLectureProgress = function(req, lectureId, isCompleted, cb) {
    const EnrolledCourse = app.models.EnrolledCourse;
    const Course = app.models.Course;
    const Lecture = app.models.Lecture;
    const LectureProgress = app.models.LectureProgress;
    const userId = req.accessToken.userId;
    Lecture.findOne({
      where: {id: lectureId},
    }, (err, lecture) => {
      if (err) return cb(err);
      if (!lecture) return cb('lecture not found');
      Course.findOne({
        where: {id: lecture.lectureId},
      }, (err, course) => {
        if (err) return cb(err);
        if (!course) return cb('course not found');
        EnrolledCourse.findOne({
          where: {courseId: course.id},
        }, (err, enrolledCourse) => {
          if (err) return cb(err);
          if (!enrolledCourse) return cb('enrolled course not found');
          LectureProgress.findOne({
            where: {lectureId: lectureId},
          }, (err, lectureProgress) => {
            if (err) return cb(err);
            if (lectureProgress) return cb('lecture progress exists');
            LectureProgress.create({
              baseUserId: userId,
              lectureId: lectureId,
              isCompleted: isCompleted,
            }, (err, lectureProgress) => {
              if (err) return cb(err);
              cb(null, lectureProgress);
            });
          });
        });
      });
    });
  };
  Baseuser.remoteMethod('postLectureProgress', {
    http: {
      path: '/lectureProgresses/',
      verb: 'post',
    },
    accepts: [
      {arg: 'req', type: 'object', http: {source: 'req'}},
      {
        arg: 'lectureId', type: 'string',
        required: true,
      },
      {arg: 'isCompleted', type: 'number', required: true},
    ],
    returns: {arg: 'lectureProgress', type: 'object'},
  });
  Baseuser.patchLectureProgress = function(req, id, isCompleted, cb) {
    const LectureProgress = app.models.LectureProgress;
    const userId = req.accessToken.userId;
    if (typeof isCompleted !== 'number') {
      cb('isCompleted is not a number');
    }
    LectureProgress.findOne({
      where: {id: id},
    }, (err, lectureProgress) => {
      if (err) return cb(err);
      if (!lectureProgress) return cb('lecture progress not found');
      lectureProgress.patchAttributes({
        isCompleted: isCompleted,
      }, (err, lectureProgress) => {
        if (err) return cb(err);
        cb(null, lectureProgress);
      });
    });
  };
  Baseuser.remoteMethod('patchLectureProgress', {
    http: {
      path: '/lectureProgresses/:id/',
      verb: 'patch',
    },
    accepts: [
      {arg: 'req', type: 'object', http: {source: 'req'}},
      {arg: 'id', type: 'string', required: true, http: {source: 'path'}},
      {arg: 'isCompleted', type: 'number', required: true},
    ],
    returns: {arg: 'lectureProgress', type: 'object'},
  });
};
