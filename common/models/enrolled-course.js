'use strict';

const app = require('../../server/server');

module.exports = function(Enrolledcourse) {
  Enrolledcourse.getLecturesFullDetails = function(req, enrolledCourseId, cb) {
    const Course = app.models.Course;
    const userId = req.accessToken.userId;
    Enrolledcourse.findOne({
      where: {
        id: enrolledCourseId,
        baseUserId: userId,
      },
    }, (err, enrolledCourse) => {
      if (err) return cb(err);
      if (!enrolledCourse) {
        return cb('user is not enrolled in the course');
      }
      Course.findOne({
        where: {id: enrolledCourse.courseId},
      }, (err, course) => {
        if (err) return cb(err);
        if (!course) {
          return cb('course not found');
        }
        Course.getLecturesFullDetails(course.id, (err, details) => {
          if (err) return cb(err);
          cb(null, details);
        });
      });
    });
  };
  Enrolledcourse.remoteMethod('getLecturesFullDetails', {
    http: {path: '/:id/lectures/', verb: 'get'},
    accepts: [
      {arg: 'req', type: 'object', http: {source: 'req'}},
      {arg: 'id', type: 'string', required: true, http: {source: 'path'}},
    ],
    returns: {arg: 'lectures', type: ['object']},
  });
  Enrolledcourse.getQuizzes = function(req, id, cb) {
    const Course = app.models.Course;
    const userId = req.accessToken.userId;
    Enrolledcourse.findOne({
      where: {
        id: id,
        baseUserId: userId,
      },
    }, (err, enrolledCourse) => {
      if (err) return cb(err);
      if (!enrolledCourse) return cb('enrolled course not found');
      Course.findOne({
        where: {id: enrolledCourse.courseId},
      }, (err, course) => {
        if (err) return cb(err);
        if (!course) return cb('course not found');
        Course.getQuizzes(course.id, (err, quizzes) => {
          if (err) cb(err);
          cb(null, quizzes);
        });
      });
    });
  };
  Enrolledcourse.remoteMethod('getQuizzes', {
    http: {path: '/:id/quizzes', verb: 'get'},
    accepts: [
      {arg: 'req', type: 'object', http: {source: 'req'}},
      {arg: 'id', type: 'string', required: true, http: {source: 'path'}},
    ],
    returns: {arg: 'quizzes', type: ['object']},
  });
  Enrolledcourse.getQuizQuestions = function(req, id, quizId, cb) {
    const Course = app.models.Course;
    const userId = req.accessToken.userId;
    Enrolledcourse.findOne({
      where: {
        id: id,
        baseUserId: userId,
      },
    }, (err, enrolledCourse) => {
      if (err) return cb(err);
      if (!enrolledCourse) return cb('enrolled course not found');
      Course.findOne({
        where: {id: enrolledCourse.courseId},
      }, (err, course) => {
        if (err) return cb(err);
        if (!course) return cb('course not found');
        Course.getQuizQuestions(course.id, quizId, (err, questions) => {
          if (err) return cb(err);
          cb(null, questions);
        });
      });
    });
  };
  Enrolledcourse.remoteMethod('getQuizQuestions', {
    http: {path: '/:id/quizzes/:quizId/questions', verb: 'get'},
    accepts: [
      {arg: 'req', type: 'object', http: {source: 'req'}},
      {arg: 'id', type: 'string', required: true, http: {source: 'path'}},
      {arg: 'quizId', type: 'string', required: true, http: {source: 'path'}},
    ],
    returns: {arg: 'questions', type: ['object']},
  });
  Enrolledcourse.getCourseDetails = function(req, id, cb) {
    const Course = app.models.Course;
    const userId = req.accessToken.userId;
    Enrolledcourse.findOne({
      where: {
        id: id,
        baseUserId: userId,
      },
    }, (err, enrolledCourse) => {
      if (err) return cb(err);
      if (!enrolledCourse) return cb('enrolled course not found');
      Course.findOne({
        where: {id: enrolledCourse.courseId},
      }, (err, course) => {
        if (err) return cb(err);
        if (!course) return cb('course not found');
        cb(null, course);
      });
    });
  };
  Enrolledcourse.remoteMethod('getCourseDetails', {
    http: {path: '/:id/details', verb: 'get'},
    accepts: [
      {arg: 'req', type: 'object', http: {source: 'req'}},
      {arg: 'id', type: 'string', required: true, http: {source: 'path'}},
    ],
    returns: {arg: 'course', type: 'object'},
  });
};
