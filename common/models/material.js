'use strict';

module.exports = function(Material) {
  Material.patchMaterial = function(material, data, cb) {
    const materialData = {
      url: data.url || material.url,
    };
    material.patchAttributes(materialData, (err, material) => {
      if (err) return cb(err);
      cb(null);
    });
  };
};
