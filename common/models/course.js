'use strict';

const app = require('../../server/server');
const async = require('async');
// TODO: add filter
module.exports = function(Course) {
  Course.createUnverified = function(data, cb) {
    Course.create({
      title: data.title,
      description: data.description,
      isVerified: 'pending',
    }, (err, ret) => {
      if (err) cb(err); // TODO: Error Code + sesuain format
      cb(null, 'Object created'); // TODO: Sesuain format
    });
  };
  Course.remoteMethod('createUnverified', {
    http: {verb: 'post'},
    accepts: {
      arg: 'data',
      type: {title: 'string', description: 'string'},
      required: true,
      http: {source: 'body'},
    },
    returns: {arg: 'message', type: 'string'},
  });
  Course.getAllVerified = function(cb) {
    Course.find({
      where: {isVerified: 'verified'},
    }, (err, ret) => {
      if (err) cb(err);
      cb(null, ret);
    });
  };
  Course.remoteMethod('getAllVerified', {
    http: {verb: 'get'},
    returns: [
      {arg: 'courses', type: ['object']},
    ],
  });
  Course.details = function(courseId, cb) {
    Course.findOne({
      where: {
        id: courseId,
        isVerified: 'verified',
      },
    }, (err, course) => {
      if (err) return cb(err);
      if (!course) return cb('course not found');
      cb(null, course);
    });
  };
  Course.remoteMethod('details', {
    http: {path: '/:id/details', verb: 'get'},
    accepts: {arg: 'id', type: 'string'},
    returns: {arg: 'course', type: 'object'},
  });
  Course.getLecturesFullDetails = function(courseId, cb) {
    const Lecture = app.models.Lecture;
    Course.details(courseId, (err, course) => {
      if (err) return cb(err);
      Lecture.find({
        where: {courseId: courseId},
      }, (err, lectures) => {
        if (err) return cb(err);
        async.mapSeries(lectures, (lecture, cbAsync) => {
          Lecture.getFullDetails(lecture.id, (err, details) => {
            if (err) return cbAsync(err);
            cbAsync(null, details);
          });
        }, (err, details) => {
          if (err) return cb(err);
          cb(null, details);
        });
      });
    });
  };
  Course.remoteMethod('getLecturesFullDetails', { // TODO: remove this
    http: {path: '/:id/lecturesFullDetails', verb: 'get'},
    accepts: {arg: 'courseId', type: 'string', required: true},
    returns: {arg: 'lectures', type: ['object']},
  });
  Course.getQuizQuestions = function(id, quizId, cb) {
    const Quiz = app.models.Quiz;
    Course.details(id, (err, course) => {
      if (err) return cb(err);
      Quiz.findOne({
        where: {id: quizId},
      }, (err, quiz) => {
        if (err) return cb(err);
        if (!quiz) {
          return cb('quiz not found');
        }
        Quiz.getQuestions(quiz.id, (err, questions) => {
          if (err) return cb(err);
          cb(null, questions);
        });
      });
    });
  };
  Course.remoteMethod('getQuizQuestions', { // TODO: remove this
    http: {path: '/:id/quizzes/:quizId/questions', verb: 'get'},
    accepts: [
      {arg: 'id', type: 'string', required: true, http: {source: 'path'}},
      {arg: 'quizId', type: 'string', required: true, http: {source: 'path'}},
    ],
    returns: {arg: 'questions', type: ['object']},
  });
  Course.getQuizzes = function(id, cb) {
    const Quiz = app.models.Quiz;
    Quiz.find({
      where: {courseId: id},
    }, (err, quizzes) => {
      if (err) return cb(err);
      cb(null, quizzes);
    });
  };
  Course.countStudents = function(id, cb) {
    const EnrolledCourse = app.models.EnrolledCourse;
    EnrolledCourse.find({
      where: {courseId: id},
    }, (err, enrolledCourses) => {
      if (err) return cb(err);
      cb(err, enrolledCourses.length);
    });
  };
  Course.getLecture = function(course, id, cb) {
    const Lecture = app.models.Lecture;
    Lecture.findOne({
      where: {
        id: id,
        courseId: course.id,
      },
    }, (err, lecture) => {
      if (err) return cb(err);
      if (!lecture) return cb('lecture not found');
      lecture.__get__lectureDetails((err, lectureDetails) => {
        if (err) return cb(err);
        if (!lectureDetails) return cb('lecture details not found');
        lectureDetails.__get__material((err, material) => {
          if (err) return cb(err);
          if (!material) return cb('material not found');
          const ret = {
            id: lecture.id,
            courseId: lecture.courseId,
            title: lecture.title,
            estimatedTime: lecture.estimatedTime,
            description: lectureDetails.description,
            url: material.url,
          };
          cb(null, ret);
        });
      });
    });
  };
  Course.postLecture = function(id, data, cb) {
    const Lecture = app.models.Lecture;
    Lecture.postLecture(id, data, (err, lecture) => {
      if (err) return cb(err);
      cb(null, lecture);
    });
  };
  Course.deleteLecture = function(id, lectureId, cb) {
    const Lecture = app.models.Lecture;
    Lecture.findOne({
      where: {
        id: lectureId,
        courseId: id,
      },
    }, (err, lecture) => {
      if (err) return cb(err);
      if (!lecture) return cb('lecture not found');
      Lecture.deleteLecture(lecture.id, (err) => {
        if (err) return cb(err);
        cb(null);
      });
    });
  };
  Course.patchLecture = function(course, lectureId, data, cb) {
    const Lecture = app.models.Lecture;
    Lecture.findOne({
      where: {
        id: lectureId,
        courseId: course.id,
      },
    }, (err, lecture) => {
      if (err) return cb(err);
      if (!lecture) return cb('lecture not found');
      Lecture.patchLecture(lecture, data, (err, lecture) => {
        if (err) return cb(err);
        cb(null, lecture);
      });
    });
  };
  Course.getVerifiedDetails = function(courseId, cb) {
    const BaseUser = app.models.BaseUser;
    Course.findOne({
      where: {
        id: courseId,
        isVerified: 'verified',
      },
    }, (err, course) => {
      if (err) return cb(err);
      if (!course) return cb('course not found');
      BaseUser.findOne({
        where: {id: course.instructorId},
      }, (err, instructor) => {
        if (err) return cb(err);
        if (!instructor) return cb('instructor not found');
        course.instructor = instructor;
        cb(null, course);
      });
    });
  };
  Course.remoteMethod('getVerifiedDetails', {
    http: {path: '/verified/:id/', verb: 'get'},
    accepts: {
      arg: 'id', type: 'string',
      required: true, http: {source: 'path'},
    },
    returns: {arg: 'course', type: 'object'},
  });
  Course.disableRemoteMethodByName('create');
  Course.disableRemoteMethodByName('replaceOrCreate');
  Course.disableRemoteMethodByName('replaceById');
  Course.disableRemoteMethodByName('createChangeStream');
  Course.disableRemoteMethodByName('updateAll');
  Course.disableRemoteMethodByName('upsertWithWhere');
};
