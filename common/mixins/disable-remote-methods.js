// Taken from https://gist.github.com/ebarault/d162892cd1873faad45faa0d08ddcdfd?ts=2 
// based on https://github.com/strongloop/loopback/issues/651#issuecomment-259540469

'use strict';

module.exports = function (Model, options) {

	if(Model && Model.sharedClass) {
		var methodsToExpose = options.expose || [];
		var methodsToHide = options.hide || [];

		if (methodsToExpose.length) {
			disableAllExcept(Model, methodsToExpose);
		}
		else if (methodsToHide.length) {
			disableOnlyTheseMethods(Model, methodsToHide);
		}
		else {
			disableAllExcept(Model);
		}
	}

/**
 * Options for disableAllExcept
 * @param Model -- The model to operate on
 * @param methodsToExpose -- An array of method names
 */
	function disableAllExcept (Model, methodsToExpose ) {
		var hiddenMethods = [];
		let explicitMethods, excludedMethods;

		// wait for all models to be attached so sharedClass.methods() returns all methods
		Model.on('attached', function (server) {
			server.on('started', function () {
				explicitMethods = getAclMethods(Model);
				excludedMethods = methodsToExpose ? methodsToExpose.concat(explicitMethods) : explicitMethods;
				Model.sharedClass.methods().forEach(disableMethod);

				if ( hiddenMethods.length ) {
					console.log( 'Hidding remote methods for model', Model.modelName, ':', hiddenMethods.join( ', ' ));
				}
			});
		});

		function disableMethod (method) {
			var methodName = method.name;
			if ( excludedMethods.indexOf(methodName) === -1 ) {
				// append prefix 'prototype.' to method name if not static
				methodName = method.isStatic? methodName: 'prototype.'+methodName;
				hiddenMethods.push(methodName);
				// disable method
				Model.disableRemoteMethodByName(methodName);
			}
		}
	}

/**
 * Options for methodsToDisable:
 * create, upsert, replaceOrCreate, upsertWithWhere, exists, findById, replaceById,
 * find, findOne, updateAll, deleteById, count, updateAttributes, createChangeStream
 * -- can also specify related method using prefixes listed above
 * and the related model name ex for Account: (prototype.__updateById__followers, prototype.__create__tags)
 * @param Model
 * @param methodsToDisable array
 */
	function disableOnlyTheseMethods (Model, methodsToDisable ) {
		methodsToDisable.forEach( function (method) {
			Model.disableRemoteMethodByName(method);
		});
		if ( methodsToDisable.length ) {
			console.log( 'Hidding explicitly remote methods for model', Model.modelName, ':', methodsToDisable.join( ', ' ));
		}
	}

	function getAclMethods (Model) {
		let authorizedMethods = [];
		const acls = Model.definition.settings.acls || [];

		acls.forEach((acl) => {
			if (acl.permission === 'ALLOW' && acl.property) {
				if (Array.isArray(acl.property)) {
					authorizedMethods = authorizedMethods.concat(acl.property);
				}
				else {
					authorizedMethods.push(acl.property);
				}
			}
		});

		if (authorizedMethods.length) {
			console.log('Exposing explicitly methods for model', Model.modelName, ':', authorizedMethods.join(', '));
		}

		return authorizedMethods;
	}

};