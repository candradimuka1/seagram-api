'use strict';

const app = require('../../server/server');

module.exports = function(Lecture) {
  Lecture.getFullDetails = function(lectureId, cb) {
    const LectureDetails = app.models.LectureDetails;
    Lecture.findOne({
      where: {id: lectureId},
    }, (err, lecture) => {
      if (err) return cb(err);
      if (!lecture) {
        return cb('lecture not found');
      }
      LectureDetails.findOne({
        where: {lectureId: lectureId},
      }, (err, lectureDetails) => {
        if (err) return cb(err);
        if (!lectureDetails) {
          return cb('lecture details not found');
        }
        LectureDetails.getLectureDetailsAndUrl(
          lectureDetails.id,
          (err, details) => {
            if (err) return cb(err);
            const ret = {
              id: lecture.id,
              courseId: lecture.courseId,
              title: lecture.title,
              estimatedTime: lecture.estimatedTime,
              description: details.description,
              url: details.url,
            };
            cb(null, ret);
          }
        );
      });
    });
  };
  Lecture.postLecture = function(courseId, data, cb) {
    const LectureDetails = app.models.LectureDetails;
    Lecture.create({
      title: data.title,
      estimatedTime: data.estimatedTime,
      courseId: courseId,
    }, (err, lecture) => {
      if (err) return cb(err);
      LectureDetails.postLectureDetails(
        lecture.id,
        data,
        (err, lectureDetail) => {
          if (err) return cb(err);
          cb(null, lecture);
        }
      );
    });
  };
  Lecture.deleteLecture = function(id, cb) {
    const LectureDetails = app.models.LectureDetails;
    Lecture.findOne({
      where: {id: id},
    }, (err, lecture) => {
      if (err) return cb(err);
      if (!lecture) return cb(null);
      LectureDetails.findOne({
        where: {lectureId: id},
      }, (err, lectureDetails) => {
        if (err) return cb(err);
        if (!lectureDetails) return cb(null);
        LectureDetails.deleteLectureDetails(lectureDetails.id, (err) => {
          if (err) return cb(err);
          Lecture.destroyById(id, (err) => {
            if (err) return cb(err);
            cb(null);
          });
        });
      });
    });
  };
  Lecture.patchLecture = function(lecture, data, cb) {
    const LectureDetails = app.models.LectureDetails;
    const lectureData = {
      title: data.title || lecture.title,
      estimatedTime: data.estimatedTime || lecture.estimatedTime,
    };
    lecture.patchAttributes(lectureData, (err, lecture) => {
      if (err) return cb(err);
      lecture.__get__lectureDetails((err, lectureDetails) => {
        if (err) return cb(err);
        LectureDetails.patchLectureDetails(
          lectureDetails,
          data,
          (err) => {
            if (err) return cb(err);
            cb(null);
          });
      });
    });
  };
};
